chrome.storage.sync.get("landregistryload", function(data) {
	if (data.landregistryload == true) {
		execute();
	}
});

function execute() {
	var formElm = document.getElementsByName("loginform")[0];
	var postCodeElm = document.getElementsByName("postcodeOrTitle")[0];
	if (formElm != null) {
		document.getElementById("username").value = "[redacted]";
		document.getElementById("password").value = "[redacted]]";

		formElm.submit();

		document.body.innerHTML = "<h1>Automatically logging you in...</h1>";
	} else if (postCodeElm != null) {
		chrome.storage.sync.set({"landregistryload": false}, null);
		chrome.storage.sync.get("landregistrypostcode", function(data) {
			var postCode = data.landregistrypostcode;
			chrome.storage.sync.set({"landregistrypostcode": null}, null);

			populatePostcodeInput(postCode);
		});
	} else if (document.body.innerHTML.indexOf("Existing session detected") > -1) {
		var links = document.getElementsByTagName("a");
		for (var i = 0; i < links.length; i++) {
			if (links[i].innerHTML == "Terminate existing login") {
				links[i].click();

				document.body.innerHTML = "<h1>Terminating previous login session...</h1>";
				break;
			}
		}
	} else {
		// Fixes going to random error page when loading
		window.location = "https://eservices.landregistry.gov.uk/mapsearch/";
	}
}

function populatePostcodeInput(postCode) {
	var postCodeElm = document.getElementsByName("postcodeOrTitle")[0];
	var searchSubmitElm = document.getElementsByName("Search")[0];
	
	if (postCodeElm.value != postCode) {
		postCodeElm.value = postCode;
	
		searchSubmitElm.click();
	}
}